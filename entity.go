package engine

import (
	"github.com/go-gl/gl/v4.1-core/gl"
)

// Entity describes a renderable entity in the game
type Entity struct {
	Game *Game

	vao      uint32
	program  uint32
	texture  uint32
	size     int32
	xUniform int32
	yUniform int32

	X      int
	Y      int
	W      int
	H      int
	Update func(e *Entity, dt, t float64)
}

func (e *Entity) draw() {
	gl.UseProgram(e.program)
	gl.BindVertexArray(e.vao)
	gl.ActiveTexture(gl.TEXTURE0)
	gl.BindTexture(gl.TEXTURE_2D, e.texture)
	gl.Uniform1f(e.xUniform, float32(e.X)/float32(e.Game.w)*2.0-1)
	gl.Uniform1f(e.yUniform, float32(e.Y)/float32(e.Game.h)*2.0-1)
	gl.DrawArrays(gl.TRIANGLE_STRIP, 0, e.size)
}

func (e *Entity) update(dt, t float64) {
	e.Update(e, dt, t)
}
