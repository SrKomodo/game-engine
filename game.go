package engine

import (
	"fmt"

	"github.com/go-gl/gl/v4.1-core/gl"
	"github.com/go-gl/glfw/v3.2/glfw"
)

// Game describes a window and the entities it contains
type Game struct {
	win      *glfw.Window
	name     string
	w        int
	h        int
	entities []*Entity
}

// NewGame creates a new Game
func NewGame(name string, w, h int) *Game {
	return &Game{nil, name, w, h, nil}
}

// Initialize sets up GLFW and OpenGL
func (g *Game) Initialize() error {
	var err error
	// Init GLFW
	err = glfw.Init()
	if err != nil {
		return fmt.Errorf("could not intialize glfw: %v", err)
	}

	// Set window settings
	glfw.WindowHint(glfw.ContextVersionMajor, 4)
	glfw.WindowHint(glfw.ContextVersionMinor, 1)
	glfw.WindowHint(glfw.Resizable, glfw.True)
	glfw.WindowHint(glfw.OpenGLProfile, glfw.OpenGLCoreProfile)
	glfw.WindowHint(glfw.OpenGLForwardCompatible, glfw.True)

	// Create window
	win, err := glfw.CreateWindow(g.w, g.h, g.name, nil, nil)
	if err != nil {
		return fmt.Errorf("could not create window: %v", err)
	}
	g.win = win
	g.win.MakeContextCurrent()

	// Init OpenGL
	err = gl.Init()
	if err != nil {
		return fmt.Errorf("could not initialize OpenGL: %v", err)
	}
	gl.ClearColor(0, 0, 0, 1)

	return nil
}

// Stop quits the game
func (g *Game) Stop() {
	g.win.SetShouldClose(true)
}

// MainLoop Starts the main loop
func (g *Game) MainLoop() {
	defer glfw.Terminate()
	var prevT float64

	// Main loop
	glfw.SetTime(0)
	for !g.win.ShouldClose() {
		t := glfw.GetTime()

		glfw.PollEvents()
		gl.Clear(gl.COLOR_BUFFER_BIT)

		// Update logic
		for _, entity := range g.entities {
			entity.update(t, t-prevT)
		}

		// Draw entities
		for _, entity := range g.entities {
			entity.draw()
		}

		g.win.SwapBuffers()

		prevT = t
	}
}

// NewEntity creates a new Entity
func (g *Game) NewEntity(
	texturePath string,
	xi, yi, wi, hi int,
	update func(e *Entity, dt, t float64),
) (*Entity, error) {
	w := float32(wi) / float32(g.w) * 2.0
	h := float32(hi) / float32(g.h) * 2.0

	vertices := []float32{
		0, h, 0, 0,
		0, 0, 0, 1,
		w, h, 1, 0,
		w, 0, 1, 1,
	}

	// Create VAO
	var vao uint32
	gl.GenVertexArrays(1, &vao)
	gl.BindVertexArray(vao)

	// Create VBO
	var vbo uint32
	gl.GenBuffers(1, &vbo)
	gl.BindBuffer(gl.ARRAY_BUFFER, vbo)
	gl.BufferData(gl.ARRAY_BUFFER, len(vertices)*4, gl.Ptr(vertices), gl.STATIC_DRAW)

	// Create program
	program, err := newProgram()
	if err != nil {
		return nil, fmt.Errorf("could not create program: %v", err)
	}
	gl.UseProgram(program)

	// Link vertex attributes
	coords := uint32(gl.GetAttribLocation(program, gl.Str("coords\x00")))
	gl.EnableVertexAttribArray(coords)
	gl.VertexAttribPointer(coords, 2, gl.FLOAT, false, 4*4, gl.PtrOffset(0))

	uv := uint32(gl.GetAttribLocation(program, gl.Str("uv\x00")))
	gl.EnableVertexAttribArray(uv)
	gl.VertexAttribPointer(uv, 2, gl.FLOAT, false, 4*4, gl.PtrOffset(2*4))

	var texture uint32
	texture, err = newTexture(texturePath)
	if err != nil {
		return nil, fmt.Errorf("could not create texture: %v", err)
	}
	textID := gl.GetUniformLocation(program, gl.Str("tex\x00"))
	gl.Uniform1i(textID, 0)

	xUniform := gl.GetUniformLocation(program, gl.Str("x\x00"))
	if xUniform == -1 {
		return nil, fmt.Errorf("could not get uniform location")
	}
	yUniform := gl.GetUniformLocation(program, gl.Str("y\x00"))
	if yUniform == -1 {
		return nil, fmt.Errorf("could not get uniform location")
	}

	entity := &Entity{
		g,
		vao, program, texture, int32(len(vertices)) / 4, xUniform, yUniform,
		xi, yi, wi, hi, update,
	}

	g.entities = append(g.entities, entity)
	return entity, nil
}
