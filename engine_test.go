package engine_test

import (
	"testing"

	"gitlab.com/srkomodo/game-engine"
)

func TestGame(t *testing.T) {
	game := engine.NewGame("Test", 512, 512)

	if err := game.Initialize(); err != nil {
		t.Errorf("Couldn't initialize game: %v", err)
	}

	if _, err := game.NewEntity("testdata/texture1.png", 0, 0, 128, 128, func(e *engine.Entity, dt, t float64) {
		e.X++
		e.Y++
		if e.X > 256 {
			e.Game.Stop()
		}
	}); err != nil {
		t.Errorf("Couldn't create entity: %v", err)
	}

	if _, err := game.NewEntity("testdata/texture2.png", 256, 256, 64, 32, func(e *engine.Entity, dt, t float64) {
		e.X++
		e.Y--
	}); err != nil {
		t.Errorf("Couldn't create entity: %v", err)
	}

	if _, err := game.NewEntity("testdata/texture3.png", 400, 0, 43, 68, func(e *engine.Entity, dt, t float64) {
		e.X--
		e.Y++
	}); err != nil {
		t.Errorf("Couldn't create entity: %v", err)
	}

	if _, err := game.NewEntity("testdata/texture4.png", 400, 512, 200, 10, func(e *engine.Entity, dt, t float64) {
		e.X--
		e.Y--
	}); err != nil {
		t.Errorf("Couldn't create entity: %v", err)
	}

	game.MainLoop()
}
